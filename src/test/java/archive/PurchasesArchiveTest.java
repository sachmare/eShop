package archive;

import cz.cvut.eshop.archive.ItemPurchaseArchiveEntry;
import cz.cvut.eshop.archive.PurchasesArchive;
import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.Order;
import cz.cvut.eshop.shop.ShoppingCart;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.*;
import org.junit.runners.MethodSorters;

import java.util.ArrayList;
import java.util.HashMap;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertEquals;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PurchasesArchiveTest {

    @Test
    public void getHowManyTimesHasBeenItemSold_itemWasNotSoldBefore()
    {
        // setup
        PurchasesArchive archive = new PurchasesArchive();
        Item item = new StandardItem(1, "name", 3.14f,
                "category", 12);
        // act
        int actual = archive.getHowManyTimesHasBeenItemSold(item);
        // assert
        assertEquals(0, actual);
    }

    @Test
    public void getHowManyTimesHasBeenItemSold_itemWasSoldBefore_v1()
    {
        // setup
        PurchasesArchive archive = new PurchasesArchive();
        Item item = new StandardItem(1, "name", 3.14f,
                "category", 12);
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(item);
        Order order = new Order(cart);
        // act
        archive.putOrderToPurchasesArchive(order);
        int actual = archive.getHowManyTimesHasBeenItemSold(item);
        // assert
        assertEquals(1, actual);
    }

    @Test
    public void getHowManyTimesHasBeenItemSold_itemWasSoldBefore_v2()
    {
        // setup
        Item item = new StandardItem(1, "name", 3.14f,
                "category", 12);
        HashMap<Integer, ItemPurchaseArchiveEntry> itemPurchaseArchiveEntryHashMap =
                new HashMap<Integer, ItemPurchaseArchiveEntry>();
        itemPurchaseArchiveEntryHashMap.put(1,
                new ItemPurchaseArchiveEntry(item));
        PurchasesArchive archive = new PurchasesArchive(
                itemPurchaseArchiveEntryHashMap,
                null
        );

        // act
        int actual = archive.getHowManyTimesHasBeenItemSold(item);
        // assert
        assertEquals(1, actual);
    }

    @Test(expected = java.lang.NullPointerException.class)
    public void getHowManyTimesHasBeenItemSold_itemIsNull()
    {
        // setup
        PurchasesArchive archive = new PurchasesArchive();
        // act
        int actual = archive.getHowManyTimesHasBeenItemSold(null);
        // assert
    }

    @Test
    public void putOrderToPurchasesArchiveTest_orderIsEmpty() {
        ShoppingCart cart = new ShoppingCart();
        Order order = new Order(cart);
        PurchasesArchive pa = new PurchasesArchive();
        Item mockItem = new StandardItem(1, "name", 30, "category", 5);

        pa.putOrderToPurchasesArchive(order);

        assertEquals(0, pa.getHowManyTimesHasBeenItemSold(mockItem));
    }

    @Test
    public void putOrderToPurchasesArchiveTest_orderIsNotEmpty() {
        Item item = new StandardItem(5, "someName", 50, "entertainment", 5);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        items.add(item);

        ShoppingCart cart = new ShoppingCart(items);
        Order order = new Order(cart);

        PurchasesArchive pa = new PurchasesArchive();
        pa.putOrderToPurchasesArchive(order);

        assertEquals(2, pa.getHowManyTimesHasBeenItemSold(item));
    }

    @BeforeClass
    public static void beforeAll()
    {
        System.out.println("before all");
    }

    @AfterClass
    public static void afterAll()
    {
        System.out.println("after all");
    }

    @Before
    public void before()
    {
        System.out.println("before");
    }

    @After
    public void after()
    {
        System.out.println("after");
    }

}
